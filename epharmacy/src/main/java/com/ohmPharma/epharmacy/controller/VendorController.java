package com.ohmPharma.epharmacy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ohmPharma.epharmacy.dao.VendorDao;
import com.ohmPharma.epharmacy.model.Vendor;
/**
 * @author Bishal Rai
 */
@Controller
public class VendorController {
	
	@Autowired
	private VendorDao vdao;
	
	@RequestMapping(value="/vendor", method=RequestMethod.GET)
	public String getVendorForm(Model model){
		model.addAttribute("vmodel", new Vendor());
		model.addAttribute("vlist", vdao.getAll());
		return "vendor";
	}
	
	@RequestMapping(value="/vendor", method=RequestMethod.POST)
	public String saveVendor(@ModelAttribute Vendor v, Model model){
		
		vdao.add(v);
		model.addAttribute("vmodel", new Vendor());
		model.addAttribute("vlist", vdao.getAll());
		return "vendor";
	}

}
