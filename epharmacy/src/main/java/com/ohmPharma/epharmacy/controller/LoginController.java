package com.ohmPharma.epharmacy.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ohmPharma.epharmacy.dao.UserDao;
import com.ohmPharma.epharmacy.model.User;
/**
 * @author Bishal Rai
 */
@Controller
public class LoginController {
	@Autowired
	private UserDao  udao;

	@RequestMapping(value = "/userlogin", method = RequestMethod.GET)
	public String getLoginForm() {
		
		
		return "Login";
	}

	@RequestMapping(value = "/usersignup", method = RequestMethod.GET)
	public String getRgtrForm() {
		
		return "Register";
	}
	
	@RequestMapping(value = "/usersignup", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute User u) {
		
		udao.userSignup(u);
		return "Login";
	}
	
	@RequestMapping(value = "/userlogin", method = RequestMethod.POST)
	public String userLogin(@ModelAttribute User u,Model model, HttpSession session)
	{
		
		if(isAdmin(u.getUsername(), u.getPassword())){
		
			 return "home";
		}
		
		System.out.println("=====login1==");
		
		User user=udao.userLogin(u.getUsername(),u.getPassword());
		System.out.println("=====login2==");
		if(user !=null){
			System.out.println("=====login3==");
			session.setAttribute("activeuser", user);
			session.setMaxInactiveInterval(8*60);
			
			model.addAttribute("user",u.getUsername());
			return "home_customer";
		}
		
		System.out.println("=====login3==");
		model.addAttribute("error","user doest not exist");
		return "Login";
	}
	
	private boolean isAdmin(String un, String psw) {

		Map<String, String> supplyMap = new HashMap<String, String>();

		supplyMap.put("admin", "admin");
		supplyMap.put("ad", "ad123");

		for (String key : supplyMap.keySet()) {

			if (key.equalsIgnoreCase(un) && supplyMap.get(key).equalsIgnoreCase(psw))

				return true;
		}

		return false;
	}
	
	@RequestMapping(value="/logout",method=RequestMethod.GET)
	public String logout(HttpSession session){
		
		session.invalidate();
		
		return "Login";
	}
	
	@RequestMapping(value="/customerList", method=RequestMethod.GET)
	public String getAllCustomer(Model model){
		
		model.addAttribute("clist", udao.getAllUser());
		
		return "customerList";
	}
	
}
