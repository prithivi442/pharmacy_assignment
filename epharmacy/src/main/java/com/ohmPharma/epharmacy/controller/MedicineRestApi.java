package com.ohmPharma.epharmacy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ohmPharma.epharmacy.dao.MedicineDao;
import com.ohmPharma.epharmacy.model.Medicine;
import com.ohmPharma.epharmacy.model.Order;
import com.ohmPharma.epharmacy.dao.OrderDao;
/**
 * @author Bishal Rai
 */
@RestController
public class MedicineRestApi {
	
	@Autowired
	private MedicineDao mdao;

	@RequestMapping(value="/restapi/medicine/list", method=RequestMethod.GET)
	public ResponseEntity<List<Medicine>> saveMedicine(@ModelAttribute Medicine med, Model model){
		
		ResponseEntity<List<Medicine>>  medicineList = new ResponseEntity<List<Medicine>>(mdao.getAllMedicine(), HttpStatus.OK);
		
		return medicineList;
	}
	
	@Autowired
	private OrderDao odao;
	@RequestMapping(value = "/restapi/order/list", method = RequestMethod.GET)
	public ResponseEntity<List<Order>> getAllOrder(Model model) {

		ResponseEntity<List<Order>>  orderlist = new ResponseEntity<List<Order>>(odao.getAll(), HttpStatus.OK);
		
		return orderlist;
	}


}
