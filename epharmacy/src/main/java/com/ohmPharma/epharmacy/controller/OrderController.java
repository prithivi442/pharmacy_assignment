package com.ohmPharma.epharmacy.controller;

import java.sql.Date;
import java.time.LocalDate;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ohmPharma.epharmacy.dao.MedicineDao;
import com.ohmPharma.epharmacy.dao.OrderDao;
import com.ohmPharma.epharmacy.model.Order;
import com.ohmPharma.epharmacy.model.User;
/**
 * @author Bishal Rai
 */
@Controller
public class OrderController {

	@Autowired
	private MedicineDao mdao;

	@Autowired
	private OrderDao odao;

	@RequestMapping(value = "/order", method = RequestMethod.GET)
	public String getRgtrForm(Model model) {

		model.addAttribute("mlist", mdao.getAllMedicine());
		model.addAttribute("morder", new Order());

		return "Order";
	}

	@RequestMapping(value = "/order", method = RequestMethod.POST)
	public String saveOrder(@ModelAttribute Order order, Model model, HttpSession session) {

		User u = (User) session.getAttribute("activeuser");
		order.setUser(u);
		order.setIsVerified("no");
		odao.add(order);

		return "redirect:order";
	}
	
	@RequestMapping(value="/{id}/deleteOrder" , method=RequestMethod.GET)
	public String deleteOrder(@PathVariable("id") int id, Model model,HttpSession session){
		
		odao.delete(id);
		User u = (User) session.getAttribute("activeuser");
		model.addAttribute("myolist", odao.getAllById(u.getId()));
		
		return "customer_order_list";
	}
	
	@RequestMapping(value="/{id}/editOrder" , method=RequestMethod.GET)
	public String editOrder(@PathVariable("id") int id, Model model){
		model.addAttribute("mlist", mdao.getAllMedicine());
		model.addAttribute("morder", odao.getById(id));
		return "editOrder";
	}
	
	@RequestMapping(value="/updateOrder" , method=RequestMethod.POST)
	public String editOrder(@ModelAttribute Order order, Model model,HttpSession session){
		 User u = (User) session.getAttribute("activeuser");
		 order.setUser(u);
		 odao.update(order);
		
			model.addAttribute("myolist", odao.getAllById(u.getId()));
			
			return "customer_order_list";
	}

	@RequestMapping(value = "/orderlist", method = RequestMethod.GET)
	public String getAllOrder(Model model) {

		model.addAttribute("olist", odao.getAll());
		return "orderList";
	}

	@RequestMapping(value = "/orderlistbyid", method = RequestMethod.GET)
	public String getOrderById(Model model, HttpSession session) {
		User u = (User) session.getAttribute("activeuser");
		model.addAttribute("myolist", odao.getAllById(u.getId()));

		return "customer_order_list";
	}

	/*
	 * verify medicine order list and send confirm email
	 */
	@RequestMapping(value = "/{id}/verifiedOrder", method = RequestMethod.GET)
	public String verifyOrder(@PathVariable("id") int id, Model model) {
		
		 Order ord = odao.getById(id);
		 ord.setIsVerified("yes");
		 ord.setDeliveredDate(Date.valueOf(LocalDate.now()));
		 odao.update(ord);
		 
		 //send email to customer
		// Mailer.send(ord.getUser().getEmail(), "Delivered Confirm", "Hello your oreder has been delivered please confirm once!!");
		 
		model.addAttribute("olist", odao.getAllVefifiedOrder());
		return "deliveredList";
	}
	
	@RequestMapping(value = "/transaction", method = RequestMethod.GET)
	public String transctionList(Model model) {
		
		model.addAttribute("olist", odao.getAllVefifiedOrder());
		return "deliveredList";
	}
	
}