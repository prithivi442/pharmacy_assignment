package com.ohmPharma.epharmacy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ohmPharma.epharmacy.dao.MedicineDao;
import com.ohmPharma.epharmacy.model.Medicine;
/**
 * @author Bishal Rai
 */
@Controller
public class MedicineController {
	
	@Autowired
	private MedicineDao mdao;
	
	@RequestMapping(value="/medicine", method=RequestMethod.GET)
	public String getMedicineForm(Model model){
		
		model.addAttribute("mmodel", new Medicine());
		
		return "medicineForm";
	}
	
	@RequestMapping(value="/medicine", method=RequestMethod.POST)
	public String saveMedicine(@ModelAttribute Medicine med, Model model){
		
		mdao.add(med);
		model.addAttribute("medList", mdao.getAllMedicine());
		
		return "medicineList";
	}
	
	@RequestMapping(value="/medicinelist", method=RequestMethod.GET)
	public String getAllMedicine(Model model){
		
		model.addAttribute("medList", mdao.getAllMedicine());
		return "medicineList";
	}
	
	//updateMed
	@RequestMapping(value="/{id}/editMed", method=RequestMethod.GET)
	public String editMed(@PathVariable("id") int id, Model model){
		model.addAttribute("mmodel", mdao.getById(id));
		
		return "editMedicine";
	}
	
	@RequestMapping(value="/updateMed", method=RequestMethod.POST)
	public String updateMed(@ModelAttribute Medicine med, Model model){
		
		mdao.update(med);
		model.addAttribute("medList", mdao.getAllMedicine());
		return "medicineList";
	}
	
	@RequestMapping(value="/{id}/deleteMed", method=RequestMethod.GET)
	public String deleteMed(@PathVariable("id") int id, Model model){
		
		mdao.delete(id);
		model.addAttribute("medList", mdao.getAllMedicine());
		return "medicineList";
	}
	

}
