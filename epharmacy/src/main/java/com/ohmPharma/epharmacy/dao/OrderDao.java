package com.ohmPharma.epharmacy.dao;

import java.util.List;

import com.ohmPharma.epharmacy.model.Order;
/**
 * @author Bishal Rai
 */
public interface OrderDao {
	
	void add(Order m);
	List<Order> getAll();
	void delete(int id);
	void update(Order m);
	Order getById(int id);
	List<Order> getAllById(int id);
	List<Order> getAllVefifiedOrder();

}
