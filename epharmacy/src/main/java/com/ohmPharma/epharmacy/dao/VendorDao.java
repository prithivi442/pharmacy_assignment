package com.ohmPharma.epharmacy.dao;

import java.util.List;

import com.ohmPharma.epharmacy.model.Order;
import com.ohmPharma.epharmacy.model.Vendor;
/**
 * @author Bishal Rai
 */
public interface VendorDao {
	
	void add(Vendor m);
	List<Vendor> getAll();

}
