package com.ohmPharma.epharmacy.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.ohmPharma.epharmacy.model.Order;
import com.ohmPharma.epharmacy.model.User;
/**
 * @author Bishal Rai
 */
@Repository
public class OrderDaoImpl implements OrderDao{
	
	@Resource
	private SessionFactory sessionFactory;
	

	@Override
	@Transactional
	public void add(Order o) {
		 
		Session session = sessionFactory.getCurrentSession();
		session.save(o);
	}

	@Override
	@Transactional
	public List<Order> getAll() {
		Session session = sessionFactory.getCurrentSession();
		Criteria crt = session.createCriteria(Order.class);
		crt.add(Restrictions.eq("isVerified", "no"));

		return crt.list();
	}
	
	@Override
	@Transactional
	public List<Order> getAllVefifiedOrder() {
		Session session = sessionFactory.getCurrentSession();
		Criteria crt = session.createCriteria(Order.class);
		crt.add(Restrictions.eq("isVerified", "yes"));

		return crt.list();
	}
	
	@Override
	@Transactional
	public List<Order> getAllById(int id) {
		
		User u =new User();
		u.setId(id);
		
		Session session = sessionFactory.getCurrentSession();
		Criteria crt = session.createCriteria(Order.class);
		crt.add(Restrictions.eq("user", u));
		
		return crt.list();
	}
	
	@Override
	@Transactional
	public void delete(int id) {
		Session session = sessionFactory.getCurrentSession();
		Order m = (Order) session.get(Order.class, id);
		session.delete(m);
	}

	@Override
	@Transactional
	public void update(Order m) {
		Session session = sessionFactory.getCurrentSession();
		session.update(m);
	}

	@Override
	@Transactional
	public Order getById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Order m = (Order) session.get(Order.class, id);

		return m;
	}

}
