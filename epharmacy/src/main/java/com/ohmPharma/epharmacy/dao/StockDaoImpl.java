package com.ohmPharma.epharmacy.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ohmPharma.epharmacy.model.Stock;

@Repository
/**
 * @author Bishal Rai
 */
public class StockDaoImpl implements StockDao {
	
	@Resource
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public void add(Stock s) {
		
		Session session = sessionFactory.getCurrentSession();
		session.save(s);
	
	}

	@Override
	@Transactional
	public List<Stock> getAllStock() {
		Session session = sessionFactory.getCurrentSession();
		Criteria crt = session.createCriteria(Stock.class);

		return crt.list();

	}
	
	

}
