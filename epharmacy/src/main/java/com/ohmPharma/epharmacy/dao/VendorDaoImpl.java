package com.ohmPharma.epharmacy.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ohmPharma.epharmacy.model.Vendor;
/**
 * @author Bishal Rai
 */
@Repository
public class VendorDaoImpl implements VendorDao{

	@Resource
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public void add(Vendor m) {
	 
		Session session = sessionFactory.getCurrentSession();
		session.save(m);
	}

	@Override
	@Transactional
	public List<Vendor> getAll() {
		
		Session session = sessionFactory.getCurrentSession();
		Criteria crt = session.createCriteria(Vendor.class);

		return crt.list();
	}

}
