package com.ohmPharma.epharmacy.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ohmPharma.epharmacy.model.Order;
import com.ohmPharma.epharmacy.model.User;
/**
 * @author Bishal Rai
 */
@Repository
public class UserDaoImpl implements UserDao {
	@Resource
	private SessionFactory sessionFactory;

	@Override
	@Transactional
	public void userSignup(User u) {
		Session session = sessionFactory.getCurrentSession();
		session.save(u);

	}

	@Override
	@Transactional
	public User userLogin(String un, String psw) {

		Session session = sessionFactory.getCurrentSession();
		Criteria crt = session.createCriteria(User.class);
		crt.add(Restrictions.eq("username", un)).add(Restrictions.eq("password", psw));
		User u = (User) crt.uniqueResult();
		return u;
	}

	@Override
	@Transactional
	public List<User> getAllUser() {
		Session session = sessionFactory.getCurrentSession();
		Criteria crt = session.createCriteria(User.class);

		return crt.list();
	}

}
