package com.ohmPharma.epharmacy.dao;

import java.util.List;

import com.ohmPharma.epharmacy.model.Medicine;
/**
 * @author Bishal Rai
 */
public interface MedicineDao {
	
	void add(Medicine s);
	List<Medicine> getAllMedicine();
	void delete(int id);
	void update(Medicine m);
	Medicine getById(int id);

}
