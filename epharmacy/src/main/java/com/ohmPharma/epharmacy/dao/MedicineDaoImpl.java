package com.ohmPharma.epharmacy.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ohmPharma.epharmacy.model.Medicine;
/**
 * @author Bishal Rai
 */
@Repository
public class MedicineDaoImpl implements MedicineDao {

	@Resource
	private SessionFactory sessionFactory;

	@Override
	@Transactional
	public void add(Medicine m) {

		Session session = sessionFactory.getCurrentSession();
		session.save(m);
	}

	@Override
	@Transactional
	public List<Medicine> getAllMedicine() {
		Session session = sessionFactory.getCurrentSession();
		Criteria crt = session.createCriteria(Medicine.class);

		return crt.list();
	}

	@Override
	@Transactional
	public void delete(int id) {
		Session session = sessionFactory.getCurrentSession();
		Medicine m = (Medicine) session.get(Medicine.class, id);
		session.delete(m);
	}

	@Override
	@Transactional
	public void update(Medicine m) {
		Session session = sessionFactory.getCurrentSession();
		session.update(m);
	}

	@Override
	@Transactional
	public Medicine getById(int id) {
		
		Session session = sessionFactory.getCurrentSession();
		Medicine m = (Medicine) session.get(Medicine.class, id);

		return m;
	}

}
