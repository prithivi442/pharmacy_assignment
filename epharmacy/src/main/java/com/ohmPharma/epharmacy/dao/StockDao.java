package com.ohmPharma.epharmacy.dao;

import java.util.List;

import com.ohmPharma.epharmacy.model.Stock;
/**
 * @author Bishal Rai
 */
public interface StockDao {

	void add(Stock m);
	List<Stock> getAllStock();

}
