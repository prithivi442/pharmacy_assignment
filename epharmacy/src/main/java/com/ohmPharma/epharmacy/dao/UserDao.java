package com.ohmPharma.epharmacy.dao;

import java.util.List;

import com.ohmPharma.epharmacy.model.User;
/**
 * @author Bishal Rai
 */
public interface UserDao {
	void userSignup(User u);
	User userLogin(String un,String psw);
	List<User> getAllUser();

}
