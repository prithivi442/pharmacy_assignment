/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ohmPharma.epharmacy.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Prithivi Raj Pandey
 */
@Entity
@Table(name = "stock_tbl")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StockTbl.findAll", query = "SELECT s FROM StockTbl s")
    , @NamedQuery(name = "StockTbl.findById", query = "SELECT s FROM StockTbl s WHERE s.id = :id")
    , @NamedQuery(name = "StockTbl.findByAvailabeQuantity", query = "SELECT s FROM StockTbl s WHERE s.availabeQuantity = :availabeQuantity")})
public class StockTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "availabeQuantity")
    private int availabeQuantity;
    @JoinColumn(name = "medicine_id", referencedColumnName = "id")
    @ManyToOne
    private MedicineTbl medicineId;

    public StockTbl() {
    }

    public StockTbl(Integer id) {
        this.id = id;
    }

    public StockTbl(Integer id, int availabeQuantity) {
        this.id = id;
        this.availabeQuantity = availabeQuantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAvailabeQuantity() {
        return availabeQuantity;
    }

    public void setAvailabeQuantity(int availabeQuantity) {
        this.availabeQuantity = availabeQuantity;
    }

    public MedicineTbl getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(MedicineTbl medicineId) {
        this.medicineId = medicineId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StockTbl)) {
            return false;
        }
        StockTbl other = (StockTbl) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lbef.epharmacy.model.StockTbl[ id=" + id + " ]";
    }
    
}
