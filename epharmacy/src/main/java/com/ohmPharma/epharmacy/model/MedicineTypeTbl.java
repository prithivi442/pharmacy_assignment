/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ohmPharma.epharmacy.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Bishal Rai
 */
@Entity
@Table(name = "medicine_type_tbl")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedicineTypeTbl.findAll", query = "SELECT m FROM MedicineTypeTbl m")
    , @NamedQuery(name = "MedicineTypeTbl.findById", query = "SELECT m FROM MedicineTypeTbl m WHERE m.id = :id")
    , @NamedQuery(name = "MedicineTypeTbl.findByMedicineType", query = "SELECT m FROM MedicineTypeTbl m WHERE m.medicineType = :medicineType")})
public class MedicineTypeTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "medicine_type")
    private String medicineType;
    @OneToMany(mappedBy = "medicineType")
    private Collection<MedicineTbl> medicineTblCollection;

    public MedicineTypeTbl() {
    }

    public MedicineTypeTbl(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMedicineType() {
        return medicineType;
    }

    public void setMedicineType(String medicineType) {
        this.medicineType = medicineType;
    }

    @XmlTransient
    public Collection<MedicineTbl> getMedicineTblCollection() {
        return medicineTblCollection;
    }

    public void setMedicineTblCollection(Collection<MedicineTbl> medicineTblCollection) {
        this.medicineTblCollection = medicineTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedicineTypeTbl)) {
            return false;
        }
        MedicineTypeTbl other = (MedicineTypeTbl) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lbef.epharmacy.model.MedicineTypeTbl[ id=" + id + " ]";
    }
    
}
