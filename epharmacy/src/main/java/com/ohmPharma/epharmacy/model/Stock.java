package com.ohmPharma.epharmacy.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
/**
 * @author Bishal Rai
 */
@Entity
@Table(name="stock_tbl")
public class Stock {
	@Id
	@GeneratedValue
	private int id;
	@OneToOne
	@JoinColumn(name="medicine_id")
	private Medicine medicine;
	private int availabeQuantity;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Medicine getMedicine() {
		return medicine;
	}
	public void setMedicine(Medicine medicine) {
		this.medicine = medicine;
	}
	public int getAvailabeQuantity() {
		return availabeQuantity;
	}
	public void setAvailabeQuantity(int availabeQuantity) {
		this.availabeQuantity = availabeQuantity;
	}
	

}
