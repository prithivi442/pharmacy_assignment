package com.ohmPharma.epharmacy.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
/**
 * @author Bishal Rai
 */
@Entity
@Table(name="order_tbl")
public class Order {
	
	@Id
	@GeneratedValue
	private int id;
	@OneToOne
	@JoinColumn(name="medicine_id")
	private Medicine medicine;
	@OneToOne
	@JoinColumn(name="user_id")
	private User user;
	private int orderQuantiry;
	private double totalPrice;
	private double price;
	private Date orderDate;
	private String isVerified;
	private Date deliveredDate;
	
	public String getIsVerified() {
		return isVerified;
	}
	public void setIsVerified(String isVerified) {
		this.isVerified = isVerified;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Medicine getMedicine() {
		return medicine;
	}
	public void setMedicine(Medicine medicine) {
		this.medicine = medicine;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getOrderQuantiry() {
		return orderQuantiry;
	}
	public void setOrderQuantiry(int orderQuantiry) {
		this.orderQuantiry = orderQuantiry;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Date getDeliveredDate() {
		return deliveredDate;
	}
	public void setDeliveredDate(Date deliveredDate) {
		this.deliveredDate = deliveredDate;
	}
	
}
