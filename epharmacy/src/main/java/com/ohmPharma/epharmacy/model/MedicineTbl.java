/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ohmPharma.epharmacy.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Prithivi Raj Pandey
 */
@Entity
@Table(name = "medicine_tbl")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedicineTbl.findAll", query = "SELECT m FROM MedicineTbl m")
    , @NamedQuery(name = "MedicineTbl.findById", query = "SELECT m FROM MedicineTbl m WHERE m.id = :id")
    , @NamedQuery(name = "MedicineTbl.findByConpanyName", query = "SELECT m FROM MedicineTbl m WHERE m.conpanyName = :conpanyName")
    , @NamedQuery(name = "MedicineTbl.findByName", query = "SELECT m FROM MedicineTbl m WHERE m.name = :name")
    , @NamedQuery(name = "MedicineTbl.findByPrice", query = "SELECT m FROM MedicineTbl m WHERE m.price = :price")
    , @NamedQuery(name = "MedicineTbl.findByQuantity", query = "SELECT m FROM MedicineTbl m WHERE m.quantity = :quantity")})
public class MedicineTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "conpanyName")
    private String conpanyName;
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "price")
    private double price;
    @Basic(optional = false)
    @Column(name = "quantity")
    private int quantity;
    @OneToMany(mappedBy = "medicineId")
    private Collection<OrderTbl> orderTblCollection;
    @JoinColumn(name = "medicine_type", referencedColumnName = "id")
    @ManyToOne
    private MedicineTypeTbl medicineType;
    @OneToMany(mappedBy = "medicineId")
    private Collection<StockTbl> stockTblCollection;

    public MedicineTbl() {
    }

    public MedicineTbl(Integer id) {
        this.id = id;
    }

    public MedicineTbl(Integer id, double price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getConpanyName() {
        return conpanyName;
    }

    public void setConpanyName(String conpanyName) {
        this.conpanyName = conpanyName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @XmlTransient
    public Collection<OrderTbl> getOrderTblCollection() {
        return orderTblCollection;
    }

    public void setOrderTblCollection(Collection<OrderTbl> orderTblCollection) {
        this.orderTblCollection = orderTblCollection;
    }

    public MedicineTypeTbl getMedicineType() {
        return medicineType;
    }

    public void setMedicineType(MedicineTypeTbl medicineType) {
        this.medicineType = medicineType;
    }

    @XmlTransient
    public Collection<StockTbl> getStockTblCollection() {
        return stockTblCollection;
    }

    public void setStockTblCollection(Collection<StockTbl> stockTblCollection) {
        this.stockTblCollection = stockTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedicineTbl)) {
            return false;
        }
        MedicineTbl other = (MedicineTbl) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lbef.epharmacy.model.MedicineTbl[ id=" + id + " ]";
    }
    
}
