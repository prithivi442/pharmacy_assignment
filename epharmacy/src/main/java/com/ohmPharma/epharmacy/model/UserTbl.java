/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ohmPharma.epharmacy.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Prithivi Raj Pandey
 */
@Entity
@Table(name = "user_tbl")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserTbl.findAll", query = "SELECT u FROM UserTbl u")
    , @NamedQuery(name = "UserTbl.findById", query = "SELECT u FROM UserTbl u WHERE u.id = :id")
    , @NamedQuery(name = "UserTbl.findByAddress", query = "SELECT u FROM UserTbl u WHERE u.address = :address")
    , @NamedQuery(name = "UserTbl.findByEmail", query = "SELECT u FROM UserTbl u WHERE u.email = :email")
    , @NamedQuery(name = "UserTbl.findByFname", query = "SELECT u FROM UserTbl u WHERE u.fname = :fname")
    , @NamedQuery(name = "UserTbl.findByLname", query = "SELECT u FROM UserTbl u WHERE u.lname = :lname")
    , @NamedQuery(name = "UserTbl.findByPassword", query = "SELECT u FROM UserTbl u WHERE u.password = :password")
    , @NamedQuery(name = "UserTbl.findByPhone", query = "SELECT u FROM UserTbl u WHERE u.phone = :phone")
    , @NamedQuery(name = "UserTbl.findByUsername", query = "SELECT u FROM UserTbl u WHERE u.username = :username")})
public class UserTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "address")
    private String address;
    @Column(name = "email")
    private String email;
    @Column(name = "fname")
    private String fname;
    @Column(name = "lname")
    private String lname;
    @Column(name = "password")
    private String password;
    @Column(name = "phone")
    private String phone;
    @Column(name = "username")
    private String username;
    @OneToMany(mappedBy = "userId")
    private Collection<OrderTbl> orderTblCollection;
    @JoinColumn(name = "user_type", referencedColumnName = "id")
    @ManyToOne
    private UserTypeTbl userType;

    public UserTbl() {
    }

    public UserTbl(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @XmlTransient
    public Collection<OrderTbl> getOrderTblCollection() {
        return orderTblCollection;
    }

    public void setOrderTblCollection(Collection<OrderTbl> orderTblCollection) {
        this.orderTblCollection = orderTblCollection;
    }

    public UserTypeTbl getUserType() {
        return userType;
    }

    public void setUserType(UserTypeTbl userType) {
        this.userType = userType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserTbl)) {
            return false;
        }
        UserTbl other = (UserTbl) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lbef.epharmacy.model.UserTbl[ id=" + id + " ]";
    }
    
}
