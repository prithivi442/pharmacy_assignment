/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ohmPharma.epharmacy.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Bishal Rai
 */
@Entity
@Table(name = "user_type_tbl")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserTypeTbl.findAll", query = "SELECT u FROM UserTypeTbl u")
    , @NamedQuery(name = "UserTypeTbl.findById", query = "SELECT u FROM UserTypeTbl u WHERE u.id = :id")
    , @NamedQuery(name = "UserTypeTbl.findByUserType", query = "SELECT u FROM UserTypeTbl u WHERE u.userType = :userType")})
public class UserTypeTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "user_type")
    private String userType;
    @OneToMany(mappedBy = "userType")
    private Collection<UserTbl> userTblCollection;

    public UserTypeTbl() {
    }

    public UserTypeTbl(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @XmlTransient
    public Collection<UserTbl> getUserTblCollection() {
        return userTblCollection;
    }

    public void setUserTblCollection(Collection<UserTbl> userTblCollection) {
        this.userTblCollection = userTblCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserTypeTbl)) {
            return false;
        }
        UserTypeTbl other = (UserTypeTbl) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lbef.epharmacy.model.UserTypeTbl[ id=" + id + " ]";
    }
    
}
