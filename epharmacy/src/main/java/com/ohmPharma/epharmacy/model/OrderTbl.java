/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ohmPharma.epharmacy.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Prithivi Raj Pandey
 */
@Entity
@Table(name = "order_tbl")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderTbl.findAll", query = "SELECT o FROM OrderTbl o")
    , @NamedQuery(name = "OrderTbl.findById", query = "SELECT o FROM OrderTbl o WHERE o.id = :id")
    , @NamedQuery(name = "OrderTbl.findByDeliveredDate", query = "SELECT o FROM OrderTbl o WHERE o.deliveredDate = :deliveredDate")
    , @NamedQuery(name = "OrderTbl.findByIsVerified", query = "SELECT o FROM OrderTbl o WHERE o.isVerified = :isVerified")
    , @NamedQuery(name = "OrderTbl.findByOrderDate", query = "SELECT o FROM OrderTbl o WHERE o.orderDate = :orderDate")
    , @NamedQuery(name = "OrderTbl.findByOrderQuantiry", query = "SELECT o FROM OrderTbl o WHERE o.orderQuantiry = :orderQuantiry")
    , @NamedQuery(name = "OrderTbl.findByPrice", query = "SELECT o FROM OrderTbl o WHERE o.price = :price")
    , @NamedQuery(name = "OrderTbl.findByTotalPrice", query = "SELECT o FROM OrderTbl o WHERE o.totalPrice = :totalPrice")})
public class OrderTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "deliveredDate")
    @Temporal(TemporalType.DATE)
    private Date deliveredDate;
    @Column(name = "isVerified")
    private String isVerified;
    @Column(name = "orderDate")
    @Temporal(TemporalType.DATE)
    private Date orderDate;
    @Basic(optional = false)
    @Column(name = "orderQuantiry")
    private int orderQuantiry;
    @Basic(optional = false)
    @Column(name = "price")
    private double price;
    @Basic(optional = false)
    @Column(name = "totalPrice")
    private double totalPrice;
    @JoinColumn(name = "medicine_id", referencedColumnName = "id")
    @ManyToOne
    private MedicineTbl medicineId;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    private UserTbl userId;

    public OrderTbl() {
    }

    public OrderTbl(Integer id) {
        this.id = id;
    }

    public OrderTbl(Integer id, int orderQuantiry, double price, double totalPrice) {
        this.id = id;
        this.orderQuantiry = orderQuantiry;
        this.price = price;
        this.totalPrice = totalPrice;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDeliveredDate() {
        return deliveredDate;
    }

    public void setDeliveredDate(Date deliveredDate) {
        this.deliveredDate = deliveredDate;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getOrderQuantiry() {
        return orderQuantiry;
    }

    public void setOrderQuantiry(int orderQuantiry) {
        this.orderQuantiry = orderQuantiry;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public MedicineTbl getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(MedicineTbl medicineId) {
        this.medicineId = medicineId;
    }

    public UserTbl getUserId() {
        return userId;
    }

    public void setUserId(UserTbl userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderTbl)) {
            return false;
        }
        OrderTbl other = (OrderTbl) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lbef.epharmacy.model.OrderTbl[ id=" + id + " ]";
    }
    
}
