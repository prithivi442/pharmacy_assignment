<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

  <%@include file="header.jsp" %>
    
     <br>
	<br>
	<br>
	<table id="myTable" class="table table-stripted">
		<thead>
			<tr class="success">

				<td>Medicine Name</td>
				<td>Company Name</td>
				<td>Quantity</td>
				<td> Price</td>
				<td>Action</td>
				</tr>
		</thead>

		<tbody>

			<c:forEach var="med" items="${medList}">
				<tr class="danger">
					<td><c:out value="${med.name}" /></td>
					<td><c:out value="${med.conpanyName}" /></td>
					<td><c:out value="${med.quantity}" /></td>
					<td><c:out value="${med.price}" /></td>
					<td>
					  <input type="button" class="btn btn-success" onclick="editMed(${med.id})" value="Edit">
					  <input type="button" class="btn btn-danger" onclick="deleteMed(${med.id})" value="Delete">
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<script type="text/javascript"> 
	

    function editMed(id){
    	
  		 	window.location = "${pageContext.request.contextPath}/"+id+"/editMed";
    }

    
    function deleteMed(id) {
		
    	  var msg = confirm("do you want to delete data?");
    	  
    	  if(msg == true){
    		  window.location = "${pageContext.request.contextPath}/"+id+"/deleteMed";
    	  }
	}
		
    
    
		$(document).ready( function () {
		    $('#myTable').DataTable();
		} );
		
		
		</script>

</body>
</html>