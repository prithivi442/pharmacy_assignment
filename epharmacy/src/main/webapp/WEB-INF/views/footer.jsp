<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<footer>
	<div class="container">

		<div class="row">

			<div class="col-md-12 col-sm-12">

				<ul class="social-icon">
					<li><a href="#" class="fa fa-facebook wow fadeInUp"
						data-wow-delay="0.2s"></a></li>
					<li><a href="#" class="fa fa-twitter wow fadeInUp"
						data-wow-delay="0.4s"></a></li>
					<li><a href="#" class="fa fa-linkedin wow fadeInUp"
						data-wow-delay="0.6s"></a></li>
					<li><a href="#" class="fa fa-instagram wow fadeInUp"
						data-wow-delay="0.8s"></a></li>
					<li><a href="#" class="fa fa-google-plus wow fadeInUp"
						data-wow-delay="1.0s"></a></li>
				</ul>

				<p class="wow fadeInUp" data-wow-delay="1s">Copyright &copy;
					2018</p>

			</div>

		</div>

	</div>
	</footer>


	<!-- Javascript  -->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/vegas.min.js"></script>
	<script src="js/modernizr.custom.js"></script>
	<script src="js/toucheffects.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/custom.js"></script>

</body>
</html>