<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@include file="header_customer.jsp"%>


	<div class="container">

		 <br>
	<br>
	<br>
	<table id="myTable" class="table table-stripted">
		<thead>
			<tr class="success">

				<td>Customer Name</td>
				<td>Medicine Name</td>
				<td>Quantity</td>
				<td> Price</td>
				<td>Total Price</td>
				<td>Order Date</td>
				<td>Delivered Date</td>
				<td>Action</td>
				</tr>
		</thead>

		<tbody>

			<c:forEach var="order" items="${myolist}">
				<tr class="danger">
					<td><c:out value="${order.user.fname}" /></td>
					<td><c:out value="${order.medicine.name}" /></td>
					<td><c:out value="${order.orderQuantiry}" /></td>
					<td><c:out value="${order.price}" /></td>
					<td><c:out value="${order.totalPrice}" /></td>
					<td><c:out value="${order.orderDate}" /></td>
					<td><c:out value="${order.deliveredDate}" /></td>
					<td>
					  <input type="button" class="btn btn-danger" onclick="deleteOrd(${order.id})" value="Delete">
					  <input type="button" class="btn btn-success" onclick="editOrd(${order.id})" value="Edit">
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	</div>
	<script type="text/javascript"> 
	

    function editOrd(id){
    	
  		 	window.location = "${pageContext.request.contextPath}/"+id+"/editOrder";
    }
    
    function deleteOrd(id) {
		
    	  var msg = confirm("do you want to delete data?");
    	  
    	  if(msg == true){
    		  window.location = "${pageContext.request.contextPath}/"+id+"/deleteOrder";
    	  }
	}
    
		$(document).ready( function () {
		    $('#myTable').DataTable();
		} );
		
		
		</script>

</body>
</html>