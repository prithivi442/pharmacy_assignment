<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

  <%@include file="header.jsp" %>
    
     <br>
	<br>
	<br>
	<table id="myTable" class="table table-stripted">
		<thead>
			<tr class="success">

				<td>First Name</td>
				<td>Last Name</td>
				<td>Address</td>
				<td> Phone</td>
				</tr>
		</thead>

		<tbody>

			<c:forEach var="cust" items="${clist}">
				<tr class="danger">
					<td><c:out value="${cust.fname}" /></td>
					<td><c:out value="${cust.lname}" /></td>
					<td><c:out value="${cust.address}" /></td>
					<td><c:out value="${cust.phone}" /></td>
					<!--  
					<td>
					  <input type="button" class="btn btn-success" onclick="verifiedOrd(${cust.id})" value="Edit">
					  <input type="button" class="btn btn-danger" onclick="deleteOrd(${cust.id})" value="Delete">
					</td>
					-->
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
	
	<script type="text/javascript"> 
	

    function editOrd(id){
    	
  		 	window.location = "${pageContext.request.contextPath}/"+id+"/editOrder";
    }

    
    function deleteOrd(id) {
		
    	  var msg = confirm("do you want to delete data?");
    	  
    	  if(msg == true){
    		  window.location = "${pageContext.request.contextPath}/"+id+"/deleteOrder";
    	  }
	}
		
    
    
		$(document).ready( function () {
		    $('#myTable').DataTable();
		} );
		
		
		</script>

</body>
</html>