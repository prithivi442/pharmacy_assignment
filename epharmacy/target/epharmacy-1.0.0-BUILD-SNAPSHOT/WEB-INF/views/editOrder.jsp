 

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@taglib uri="http://www.springframework.org/tags/form"  prefix="spring" %>
<%@include file="header_customer.jsp"%>
    <div class="container" >    
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title">Medicine Order Details</div>
                    </div>     

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <spring:form id="loginform" class="form-horizontal" role="form" action="${pageContext.request.contextPath}/updateOrder" method="post" modelAttribute="morder">
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon">Medicine Name</i></span>
                                      <spring:select path="medicine.id" style="height:35px;">
                                         <spring:option value="0">---select medicine---</spring:option>
                                         <c:forEach var="med" items="${mlist}">
                                               <spring:option value="${med.id}">${med.name}</spring:option>
                                         </c:forEach>
                                                         
                                  </spring:select>                     
                                    </div>
                                 
                                    
                                    <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon">Price</i></span>
                                        <spring:input id="price" type="text" class="form-control" path="price"  placeholder="Price"/>                                        
                                    </div>
                                    
                                    <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon">Quantity</i></span>
                                        <spring:input id="quantity" type="text" class="form-control" path="orderQuantiry"   placeholder="Quantity" onchange="calculate();" />                                        
                                    </div>
                             
                             
                               <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon">Total Price</i></span>
                                        <spring:input id="total" type="text" class="form-control" path="totalPrice"   placeholder="total price "/>                                        
                                    </div>
                                    
                                    
                                 <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon">Date</i></span>
                                        <spring:input id="login-username" class="form-control" path="orderDate" type="date" placeholder="Date "/>                                        
                                    </div>   

                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">
                                    
                                     <input type="submit" value="Submit" class="btn btn-success">
                                    </div>
                                </div>
                                
                                <spring:hidden path="id"/>
                                <spring:hidden path="${med.id}"/>
                            </spring:form>     

                        </div>                     
                    </div>  
        </div>
       
    </div>
    
    <script type="text/javascript">
	//Multiple price and quantity
    
    function calculate() {
 			
         var myBox1 = document.getElementById('price').value; 
         var myBox2 = document.getElementById('quantity').value;
         var result = document.getElementById('total'); 
         var myResult = myBox1 * myBox2;
           document.getElementById('total').value = myResult;

     }
</script>
