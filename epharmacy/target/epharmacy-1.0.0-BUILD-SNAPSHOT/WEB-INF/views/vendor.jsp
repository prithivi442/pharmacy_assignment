 

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form"  prefix="spring" %>

<%@include file="header.jsp"%>
  
    <div class="container" >    
        <div id="loginbox" style="margin-top:60px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title">Vendors</div>
                    </div>     

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <spring:form id="loginform" class="form-horizontal" role="form" action="vendor" method="post" modelAttribute="vmodel">
                                    
                                    <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon">Vendor Name</span>
                                        <spring:input  type="text" class="form-control" path="vendorName"  placeholder="Vendor Name" required="true"/>                                        
                                    </div>
                                    
                                    <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon">Address</span>
                                        <spring:input  type="text" class="form-control" path="address"  placeholder="Vendor Address" required="true" />                                        
                                    </div>
                                    
                                    <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon">Phone</span>
                                        <spring:input  type="text" class="form-control" path="contact"  placeholder="Vendor Phone" required="true"/>                                        
                                    </div>

                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->
                                    <div class="col-sm-12 controls">
                                     <input type="submit" value="Submit" class="btn btn-success">
                                    </div>
                                </div>
                            </spring:form>     

                        </div>  
                        
                        
                        
                                           
                    </div>  
        </div>
        
        
         <br>
	<br>
	<br>
	<table id="myTable" class="table table-stripted">
		<thead>
			<tr class="success">

				<td>Vendor Name</td>
				<td>Contact</td>
				<td>Address</td>
				</tr>
		</thead>

		<tbody>

			<c:forEach var="vend" items="${vlist}">
				<tr class="danger">
					<td><c:out value="${vend.vendorName}" /></td>
					<td><c:out value="${vend.contact}" /></td>
					<td><c:out value="${vend.address}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
	
	<script type="text/javascript"> 
	

    function editOrd(id){
    	
  		 	window.location = "${pageContext.request.contextPath}/"+id+"/editOrder";
    }

    
    function deleteOrd(id) {
		
    	  var msg = confirm("do you want to delete data?");
    	  
    	  if(msg == true){
    		  window.location = "${pageContext.request.contextPath}/"+id+"/deleteOrder";
    	  }
	}
		
    
    
		$(document).ready( function () {
		    $('#myTable').DataTable();
		} );
		
		
		</script>
       
    
    