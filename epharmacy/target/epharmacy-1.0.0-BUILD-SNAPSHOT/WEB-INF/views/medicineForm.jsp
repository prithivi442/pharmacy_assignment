 <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@taglib uri="http://www.springframework.org/tags/form"  prefix="spring" %>
  <%@include file="header.jsp" %>
  <br><br><br>

    <div class="container" >    
        <div id="loginbox" style="margin-top:50px; width: 800px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title">Medicine Details</div>
                    </div>     

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <spring:form id="loginform" class="form-horizontal" role="form" action="medicine" method="post" modelAttribute="mmodel">
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon">Medicine Name</i></span>
                                        <spring:input id="login-username" type="text" class="form-control" path="name" value="" placeholder="medicine Name"/>                                        
                                    </div>
                                
                                
                                 <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon">Company Name</i></span>
                                        <spring:input id="login-username" type="text" class="form-control" path="conpanyName" value="" placeholder="Company Name"/>                                        
                                    </div>
                                    
                                    <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon">Quantity</i></span>
                                        <spring:input id="login-username" type="text" class="form-control" path="quantity" value="" placeholder="quantity "/>                                        
                                    </div>
                             
                             
                               <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon">Price</i></span>
                                        <spring:input id="login-username" type="text" class="form-control" path="price" value="" placeholder="price "/>                                        
                                    </div>
                                    


                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">
                                    
                                     <input type="submit" value="Submit" class="btn btn-success">
                                    </div>
                                </div>
                            </spring:form>     

                        </div>                     
                    </div>  
        </div>
        
         </div>
