/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50623
 Source Host           : 192.168.40.215:3306
 Source Schema         : pharmacydb

 Target Server Type    : MySQL
 Target Server Version : 50623
 File Encoding         : 65001

 Date: 30/06/2019 16:50:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for medicine_tbl
-- ----------------------------
DROP TABLE IF EXISTS `medicine_tbl`;
CREATE TABLE `medicine_tbl`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conpanyName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `medicine_type` int(5) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `medicine_type_fk`(`medicine_type`) USING BTREE,
  CONSTRAINT `medicine_type_fk` FOREIGN KEY (`medicine_type`) REFERENCES `medicine_type_tbl` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of medicine_tbl
-- ----------------------------
INSERT INTO `medicine_tbl` VALUES (1, 'Kathmandu Pharma', 'Pantop', 100, 2, NULL);

-- ----------------------------
-- Table structure for medicine_type_tbl
-- ----------------------------
DROP TABLE IF EXISTS `medicine_type_tbl`;
CREATE TABLE `medicine_type_tbl`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `medicine_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for order_tbl
-- ----------------------------
DROP TABLE IF EXISTS `order_tbl`;
CREATE TABLE `order_tbl`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deliveredDate` date NULL DEFAULT NULL,
  `isVerified` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `orderDate` date NULL DEFAULT NULL,
  `orderQuantiry` int(11) NOT NULL,
  `price` double NOT NULL,
  `totalPrice` double NOT NULL,
  `medicine_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKE8AE1F4D3E38071F`(`medicine_id`) USING BTREE,
  INDEX `FKE8AE1F4DA6856BFF`(`user_id`) USING BTREE,
  CONSTRAINT `FKE8AE1F4D3E38071F` FOREIGN KEY (`medicine_id`) REFERENCES `medicine_tbl` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKE8AE1F4DA6856BFF` FOREIGN KEY (`user_id`) REFERENCES `user_tbl` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of order_tbl
-- ----------------------------
INSERT INTO `order_tbl` VALUES (1, '2019-06-28', 'true', '2019-06-20', 1000, 100, 100000, 1, 2);

-- ----------------------------
-- Table structure for stock_tbl
-- ----------------------------
DROP TABLE IF EXISTS `stock_tbl`;
CREATE TABLE `stock_tbl`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `availabeQuantity` int(11) NOT NULL,
  `medicine_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK4B9A6B153E38071F`(`medicine_id`) USING BTREE,
  CONSTRAINT `FK4B9A6B153E38071F` FOREIGN KEY (`medicine_id`) REFERENCES `medicine_tbl` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user_tbl
-- ----------------------------
DROP TABLE IF EXISTS `user_tbl`;
CREATE TABLE `user_tbl`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_type` int(5) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_type_fk`(`user_type`) USING BTREE,
  CONSTRAINT `user_type_fk` FOREIGN KEY (`user_type`) REFERENCES `user_type_tbl` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_tbl
-- ----------------------------
INSERT INTO `user_tbl` VALUES (1, NULL, 'test@test.com', 'ds', 'rtrt', '12345', NULL, 'test', NULL);
INSERT INTO `user_tbl` VALUES (2, NULL, 'bishal@gmail.com', 'Bishal', 'Rai', '12345', NULL, 'bishal.rai', NULL);

-- ----------------------------
-- Table structure for user_type_tbl
-- ----------------------------
DROP TABLE IF EXISTS `user_type_tbl`;
CREATE TABLE `user_type_tbl`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for vendor_tbl
-- ----------------------------
DROP TABLE IF EXISTS `vendor_tbl`;
CREATE TABLE `vendor_tbl`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `vendorName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
